<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Inertia\Inertia;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Inertia\Response
     */
    public function index()
    {
        $products = Product::with('brand', 'categories')->get();
        $brands = Brand::all();
        $categories = Category::all();

        return Inertia::render('Product/Index', [
            'products' => $products,
            'brands' => $brands,
            'categories' => $categories
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Inertia\Response
     */
    public function create()
    {
        return Inertia::render('Product/Create', [
            'categories' => Category::all(),
            'brands' => Brand::all()
        ]);
    }

    public function uploadImage()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        $product = new Product();
        $product->name = $request->name;
        $product->description = $request->description;
        $product->brand_id = $request->brand_id;
        $product->model = $request->model;
        $product->price = $request->price;

        $destino = '/img/products';
        $image = $request->hasFile('image');
        if ($image) {
            $imageFile = $request->file('image');
            $filename = $request->name . '_' . $request->model . '_' . $imageFile->getClientOriginalExtension();
            $imageFile->move($destino, $filename);
            $product->image = $destino . '/' . $filename;
        }

        $product->save();

        $product->categories()->sync($request->categories);
        DB::commit();

        return redirect('/product');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Inertia\Response
     */
    public function show($id)
    {
        $product = Product::with('brand', 'categories')->findOrFail($id);
        return Inertia::render('Product/Show', [
            'product' => $product
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Inertia\Response
     */
    public function edit($id)
    {
        $product = Product::with('brand', 'categories')->findOrFail($id);

        return Inertia::render('Product/Edit', [
            'product' => $product,
            'categories' => Category::all(),
            'brands' => Brand::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        $product = Product::findOrFail($id);
        $product->name = $request->name;
        $product->description = $request->description;
        $product->brand_id = $request->brand_id;
        $product->model = $request->model;
        $product->price = $request->price;

        $pic = $request->file('file');
        $destino = '/img/products';
        if ($pic) {
            $this->removeProductImage($product);
            $pic = $request->file('file');
            $filename = $request->name  . '.' . $request->model . '_' . $pic->getClientOriginalExtension();
            $pic->move($destino, $filename);
            $product->image = $destino . '/' . $filename;
        }

        $product->save();

        $product->categories()->sync($request->categories);
        DB::commit();

        return redirect('/product');
    }
    private function removeProductImage(Product $product)
    {
        if (!empty($product->image) && file_exists(public_path($product->image))) {
            unlink(public_path($product->image));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        $product = Product::findOrFail($id);
        $product -> categories()->detach();
        $product->delete();
        DB::commit();

        return back();
    }
}
