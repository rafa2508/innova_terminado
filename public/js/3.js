(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[3],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Auth/Login.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/Pages/Auth/Login.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_ProductComponent__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../components/ProductComponent */ "./resources/js/components/ProductComponent.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    ProductComponent: _components_ProductComponent__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      loginDialogVisible: false,
      log_form: {
        username: '',
        password: ''
      },
      log_rules: {
        username: {
          required: true,
          message: 'Su usuario es requerido'
        },
        password: {
          required: true,
          message: 'Su contraseña es requerida'
        }
      },
      registerDialogVisible: false,
      reg_form: {
        name: '',
        username: '',
        password: '',
        password_confirmation: ''
      },
      reg_rules: {
        username: {
          required: true,
          message: 'Su nombre es requerido'
        },
        name: {
          required: true,
          message: 'Su usuario es requerido'
        },
        password: {
          required: true,
          message: 'Su contraseña es requerida'
        },
        password_confirmation: {
          required: true,
          message: 'Debe verificar su contraseña'
        }
      },
      products: []
    };
  },
  methods: {
    login: function login() {
      var _this = this;

      this.$refs.log_form.validate(function () {
        _this.$inertia.post('/login', {
          username: _this.log_form.username,
          password: _this.log_form.password
        });
      });
    },
    register: function register() {
      var _this2 = this;

      this.$refs.reg_form.validate(function () {
        _this2.$inertia.post('/register', {
          name: _this2.reg_form.name,
          username: _this2.reg_form.username,
          password: _this2.reg_form.password,
          password_confirmation: _this2.reg_form.password_confirmation
        });
      });
    }
  },
  mounted: function mounted() {
    if (this.$page.products) {
      this.products = this.$page.products;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/ProductComponent.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/ProductComponent.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'product',
  props: {
    product: Object
  },
  methods: {
    addToCart: function addToCart(id) {
      var _this = this;

      this.$confirm('Agregar este producto a su carrito?', {
        confirmButtonText: 'Si',
        cancelButtonText: 'Cancelar',
        cancelButtonClass: 'el-button--info',
        type: 'info'
      }).then()(function () {
        _this.$inertia.post('/cart/' + id, {
          product_id: id
        }).then(function () {
          _this.$message({
            type: 'success',
            message: 'Agregado al carrito'
          });
        });
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Auth/Login.vue?vue&type=template&id=a2ac2cea&":
/*!********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/Pages/Auth/Login.vue?vue&type=template&id=a2ac2cea& ***!
  \********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "el-header",
        { staticClass: "login-header" },
        [
          _c("el-row", [
            _c("span", { staticClass: "page-title" }, [_vm._v("Innova")]),
            _vm._v(" "),
            _c(
              "span",
              { staticClass: "header-right" },
              [
                _c(
                  "el-link",
                  {
                    staticStyle: { color: "white" },
                    attrs: { underline: false },
                    on: {
                      click: function($event) {
                        _vm.loginDialogVisible = true
                      }
                    }
                  },
                  [_vm._v("Login")]
                ),
                _vm._v(" "),
                _c("el-divider", { attrs: { direction: "vertical" } }),
                _vm._v(" "),
                _c(
                  "el-link",
                  {
                    staticStyle: { color: "white" },
                    attrs: { underline: false },
                    on: {
                      click: function($event) {
                        _vm.registerDialogVisible = true
                      }
                    }
                  },
                  [_vm._v("Registrarse")]
                )
              ],
              1
            )
          ])
        ],
        1
      ),
      _vm._v(" "),
      _c("el-header", {
        staticStyle: { height: "5px", "background-color": "#9615db" }
      }),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "grid-container2" },
        _vm._l(_vm.products, function(p, index) {
          return _c(
            "div",
            { key: index, staticClass: "grid-item" },
            [_c("product-component", { attrs: { product: p } })],
            1
          )
        }),
        0
      ),
      _vm._v(" "),
      _c(
        "el-dialog",
        {
          attrs: { title: "Login", visible: _vm.loginDialogVisible },
          on: {
            "update:visible": function($event) {
              _vm.loginDialogVisible = $event
            }
          }
        },
        [
          _c(
            "el-form",
            {
              ref: "log_form",
              attrs: { model: _vm.log_form, rules: _vm.log_rules }
            },
            [
              _c(
                "el-form-item",
                { attrs: { label: "Usuario", prop: "username" } },
                [
                  _c("el-input", {
                    attrs: { placeholder: "Usuario" },
                    model: {
                      value: _vm.log_form.username,
                      callback: function($$v) {
                        _vm.$set(_vm.log_form, "username", $$v)
                      },
                      expression: "log_form.username"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "el-form-item",
                { attrs: { label: "Contraseña", prop: "password" } },
                [
                  _c("el-input", {
                    attrs: {
                      type: "password",
                      "show-password": "",
                      placeholder: "Contraseña"
                    },
                    model: {
                      value: _vm.log_form.password,
                      callback: function($$v) {
                        _vm.$set(_vm.log_form, "password", $$v)
                      },
                      expression: "log_form.password"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _vm.$page.prop.errors
                ? _c(
                    "div",
                    _vm._l(_vm.$page.prop.errors, function(error) {
                      return _c(
                        "div",
                        _vm._l(error, function(er, index) {
                          return _c("el-alert", {
                            key: index,
                            staticStyle: { "margin-bottom": "5px" },
                            attrs: { type: "error", title: er, closable: false }
                          })
                        }),
                        1
                      )
                    }),
                    0
                  )
                : _vm._e(),
              _vm._v(" "),
              _c(
                "div",
                { staticStyle: { "text-align": "center" } },
                [
                  _c(
                    "el-button",
                    {
                      attrs: { type: "primary" },
                      on: {
                        click: function($event) {
                          return _vm.login()
                        }
                      }
                    },
                    [_vm._v("Entrar")]
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "el-dialog",
        {
          attrs: { title: "Registrarse", visible: _vm.registerDialogVisible },
          on: {
            "update:visible": function($event) {
              _vm.registerDialogVisible = $event
            }
          }
        },
        [
          _c(
            "el-form",
            {
              ref: "reg_form",
              attrs: { model: _vm.reg_form, rules: _vm.reg_rules }
            },
            [
              _c(
                "el-form-item",
                { attrs: { label: "Nombre", prop: "name" } },
                [
                  _c("el-input", {
                    attrs: { placeholder: "Nombre" },
                    model: {
                      value: _vm.reg_form.name,
                      callback: function($$v) {
                        _vm.$set(_vm.reg_form, "name", $$v)
                      },
                      expression: "reg_form.name"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "el-form-item",
                { attrs: { label: "Usuario", prop: "username" } },
                [
                  _c("el-input", {
                    attrs: { placeholder: "Usuario" },
                    model: {
                      value: _vm.reg_form.username,
                      callback: function($$v) {
                        _vm.$set(_vm.reg_form, "username", $$v)
                      },
                      expression: "reg_form.username"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "el-row",
                { attrs: { gutter: 20 } },
                [
                  _c("el-col", { attrs: { span: 12 } }),
                  _vm._v(" "),
                  _c(
                    "el-form-item",
                    { attrs: { label: "Contraseña", prop: "password" } },
                    [
                      _c("el-input", {
                        attrs: {
                          type: "password",
                          "show-password": "",
                          placeholder: "Contraseña"
                        },
                        model: {
                          value: _vm.reg_form.password,
                          callback: function($$v) {
                            _vm.$set(_vm.reg_form, "password", $$v)
                          },
                          expression: "reg_form.password"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c("el-col", { attrs: { span: 12 } }),
                  _vm._v(" "),
                  _c(
                    "el-form-item",
                    {
                      attrs: {
                        label: "Verificar Contraseña",
                        prop: "password_confirmation"
                      }
                    },
                    [
                      _c("el-input", {
                        attrs: {
                          type: "password",
                          "show-password": "",
                          placeholder: " Verificar Contraseña"
                        },
                        model: {
                          value: _vm.reg_form.password_confirmation,
                          callback: function($$v) {
                            _vm.$set(_vm.reg_form, "password_confirmation", $$v)
                          },
                          expression: "reg_form.password_confirmation"
                        }
                      })
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _vm.$page.prop.errors
                ? _c(
                    "div",
                    _vm._l(_vm.$page.prop.errors, function(error) {
                      return _c(
                        "div",
                        _vm._l(error, function(er, index) {
                          return _c("el-alert", {
                            key: index,
                            staticStyle: { "margin-bottom": "5px" },
                            attrs: { type: "error", title: er, closable: false }
                          })
                        }),
                        1
                      )
                    }),
                    0
                  )
                : _vm._e(),
              _vm._v(" "),
              _c(
                "div",
                { staticStyle: { "text-align": "center" } },
                [
                  _c(
                    "el-button",
                    {
                      attrs: { type: "primary" },
                      on: {
                        click: function($event) {
                          return _vm.register()
                        }
                      }
                    },
                    [_vm._v("Registrar")]
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/ProductComponent.vue?vue&type=template&id=9e91ef8e&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/ProductComponent.vue?vue&type=template&id=9e91ef8e& ***!
  \*******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "el-card",
        { attrs: { "body-style": { padding: "0px" } } },
        [
          _vm.product.image
            ? _c("el-image", {
                attrs: { src: "product.image", fit: "scale-down" }
              })
            : _c(
                "div",
                { staticStyle: { "text-align": "center" } },
                [
                  _c("i", {
                    staticClass: "fas fa-image fa-6x",
                    staticStyle: { "margin-top": "20px" }
                  }),
                  _vm._v(" "),
                  _c("el-divider")
                ],
                1
              ),
          _vm._v(" "),
          _c(
            "div",
            { staticStyle: { padding: "14px" } },
            [
              _c(
                "el-row",
                { attrs: { gutter: 5 } },
                [
                  _c("el-col", { attrs: { span: 16 } }, [
                    _c("span", [
                      _c("strong", [_vm._v(_vm._s(_vm.product.name))])
                    ])
                  ]),
                  _vm._v(" "),
                  _c(
                    "el-col",
                    {
                      staticStyle: { "text-align": "right" },
                      attrs: { span: 8 }
                    },
                    [
                      _c("el-tag", { attrs: { effect: "dark" } }, [
                        _vm._v(_vm._s(_vm.product.brand.name))
                      ])
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c("p", [_vm._v(_vm._s(_vm.product.description))]),
          _vm._v(" "),
          _c("el-divider"),
          _vm._v(" "),
          _c(
            "el-row",
            { staticStyle: { "text-align": "right" } },
            [
              _c(
                "inertia-link",
                { attrs: { href: "/product/" + _vm.product.id } },
                [
                  _c(
                    "span",
                    { staticStyle: { "font-size": "20px", color: "#f79f54" } },
                    [_c("i", { staticClass: "fas fa-eye" })]
                  )
                ]
              ),
              _vm._v(" "),
              _c(
                "span",
                {
                  staticStyle: { "font-size": "20px", color: "#00b9a2" },
                  on: {
                    click: function($event) {
                      return _vm.addToCart(_vm.product.id)
                    }
                  }
                },
                [_c("i", { staticClass: "fas fa-cart-plus" })]
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () { injectStyles.call(this, this.$root.$options.shadowRoot) }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ "./resources/js/Pages/Auth/Login.vue":
/*!*******************************************!*\
  !*** ./resources/js/Pages/Auth/Login.vue ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Login_vue_vue_type_template_id_a2ac2cea___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Login.vue?vue&type=template&id=a2ac2cea& */ "./resources/js/Pages/Auth/Login.vue?vue&type=template&id=a2ac2cea&");
/* harmony import */ var _Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Login.vue?vue&type=script&lang=js& */ "./resources/js/Pages/Auth/Login.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Login_vue_vue_type_template_id_a2ac2cea___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Login_vue_vue_type_template_id_a2ac2cea___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/Auth/Login.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/Auth/Login.vue?vue&type=script&lang=js&":
/*!********************************************************************!*\
  !*** ./resources/js/Pages/Auth/Login.vue?vue&type=script&lang=js& ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Login.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Auth/Login.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/Pages/Auth/Login.vue?vue&type=template&id=a2ac2cea&":
/*!**************************************************************************!*\
  !*** ./resources/js/Pages/Auth/Login.vue?vue&type=template&id=a2ac2cea& ***!
  \**************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_template_id_a2ac2cea___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Login.vue?vue&type=template&id=a2ac2cea& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Auth/Login.vue?vue&type=template&id=a2ac2cea&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_template_id_a2ac2cea___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_template_id_a2ac2cea___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/ProductComponent.vue":
/*!******************************************************!*\
  !*** ./resources/js/components/ProductComponent.vue ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ProductComponent_vue_vue_type_template_id_9e91ef8e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ProductComponent.vue?vue&type=template&id=9e91ef8e& */ "./resources/js/components/ProductComponent.vue?vue&type=template&id=9e91ef8e&");
/* harmony import */ var _ProductComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ProductComponent.vue?vue&type=script&lang=js& */ "./resources/js/components/ProductComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ProductComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ProductComponent_vue_vue_type_template_id_9e91ef8e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ProductComponent_vue_vue_type_template_id_9e91ef8e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/ProductComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/ProductComponent.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/js/components/ProductComponent.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./ProductComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/ProductComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/ProductComponent.vue?vue&type=template&id=9e91ef8e&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/ProductComponent.vue?vue&type=template&id=9e91ef8e& ***!
  \*************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductComponent_vue_vue_type_template_id_9e91ef8e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./ProductComponent.vue?vue&type=template&id=9e91ef8e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/ProductComponent.vue?vue&type=template&id=9e91ef8e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductComponent_vue_vue_type_template_id_9e91ef8e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductComponent_vue_vue_type_template_id_9e91ef8e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);