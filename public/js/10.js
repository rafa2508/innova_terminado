(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[10],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Product/Show.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/Pages/Product/Show.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Shared_Layout__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../Shared/Layout */ "./resources/js/Shared/Layout.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Layout: _Shared_Layout__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      product: {}
    };
  },
  methods: {
    addToCart: function addToCart(id) {
      var _this = this;

      this.$confirm('Agregar este producto a su carrito?', {
        confirmButtonText: 'Ok',
        cancelButtonText: 'Cancelar',
        cancelButtonClass: 'el-button--info',
        type: 'info'
      }).then(function () {
        _this.$inertia.post('/cart/' + id, {
          product_id: id
        }).then(function () {
          _this.$message({
            type: 'success',
            message: 'Agregado al carrito'
          });
        });
      });
    }
  },
  mounted: function mounted() {
    if (this.$page.product) {
      this.product = this.$page.product;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Shared/Layout.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/Shared/Layout.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_avatar__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-avatar */ "./node_modules/vue-avatar/dist/vue-avatar.min.js");
/* harmony import */ var vue_avatar__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue_avatar__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'layout',
  components: {
    Avatar: vue_avatar__WEBPACK_IMPORTED_MODULE_0___default.a
  },
  data: function data() {
    return {
      menuItems: [],
      name: '',
      role: ''
    };
  },
  methods: {},
  mounted: function mounted() {
    if (this.$page.prop.auth.user) {
      this.name = this.$page.prop.auth.user.name;
      this.role = this.$page.prop.auth.user.role_id;
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Product/Show.vue?vue&type=template&id=6fc30034&":
/*!**********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/Pages/Product/Show.vue?vue&type=template&id=6fc30034& ***!
  \**********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("Layout", [
    _c(
      "div",
      [
        _c("el-row", [
          _c(
            "span",
            { staticStyle: { color: "orange", "font-size": "20px" } },
            [_c("strong", [_vm._v(_vm._s(_vm.product.brand.name))])]
          ),
          _vm._v(" "),
          _c("span", { staticStyle: { "font-size": "16px" } }, [
            _vm._v("- " + _vm._s(_vm.product.name))
          ]),
          _vm._v(" "),
          _c("span")
        ]),
        _vm._v(" "),
        _c("el-row", [
          _c("span", [
            _c("strong", [_vm._v("Modelo: ")]),
            _vm._v(" " + _vm._s(_vm.product.model) + " ")
          ])
        ]),
        _vm._v(" "),
        _c("el-divider"),
        _vm._v(" "),
        _c(
          "el-row",
          { attrs: { gutter: 20 } },
          [
            _c(
              "el-col",
              { attrs: { span: 12 } },
              [
                _vm.product.image
                  ? _c("el-image", {
                      staticStyle: { "margin-top": "5px", height: "400px" },
                      attrs: { src: _vm.product.image, fit: "scale-down" }
                    })
                  : _c(
                      "div",
                      { staticStyle: { "text-aling": "center" } },
                      [
                        _c("el-image", {
                          staticStyle: { "margin-top": "5px", height: "400px" },
                          attrs: {
                            fit: "scale-down",
                            src:
                              "https://thumbs.dreamstime.com/b/no-image-available-icon-flat-vector-no-image-available-icon-flat-vector-illustration-132482953.jpg"
                          }
                        })
                      ],
                      1
                    )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "el-col",
              { attrs: { span: 12 } },
              [
                _c(
                  "el-row",
                  [
                    _c("el-col", { attrs: { span: 8 } }, [
                      _c("h3", [_vm._v("Marca")])
                    ]),
                    _vm._v(" "),
                    _c("el-col", { attrs: { span: 16 } }, [
                      _vm._v(
                        "\n                        " +
                          _vm._s(_vm.product.brand.name) +
                          "\n                    "
                      )
                    ])
                  ],
                  1
                ),
                _vm._v(" "),
                _c("el-divider"),
                _vm._v(" "),
                _c(
                  "el-row",
                  [
                    _c("el-col", { attrs: { span: 8 } }, [
                      _c("h3", [_vm._v("Marca")])
                    ]),
                    _vm._v(" "),
                    _c("el-col", { attrs: { span: 16 } }, [
                      _vm._v(
                        "\n                        " +
                          _vm._s(_vm.product.name) +
                          "\n                    "
                      )
                    ])
                  ],
                  1
                ),
                _vm._v(" "),
                _c("el-divider"),
                _vm._v(" "),
                _c(
                  "el-row",
                  [
                    _c("el-col", { attrs: { span: 8 } }, [
                      _c("h3", [_vm._v("Modelo")])
                    ]),
                    _vm._v(" "),
                    _c("el-col", { attrs: { span: 16 } }, [
                      _vm._v(
                        "\n                        " +
                          _vm._s(_vm.product.model) +
                          "\n                    "
                      )
                    ])
                  ],
                  1
                ),
                _vm._v(" "),
                _c("el-divider"),
                _vm._v(" "),
                _c(
                  "el-row",
                  [
                    _c("el-col", { attrs: { span: 8 } }, [
                      _c("h3", [_vm._v("Precio")])
                    ]),
                    _vm._v(" "),
                    _c("el-col", { attrs: { span: 16 } }, [
                      _c("strong", [_vm._v("$ ")]),
                      _vm._v(
                        " " +
                          _vm._s(_vm.product.price) +
                          "\n                    "
                      )
                    ])
                  ],
                  1
                ),
                _vm._v(" "),
                _c("el-divider"),
                _vm._v(" "),
                _c(
                  "el-row",
                  [
                    _c("el-col", { attrs: { span: 8 } }, [
                      _c("h3", [_vm._v("Categorias")])
                    ]),
                    _vm._v(" "),
                    _c(
                      "el-col",
                      { attrs: { span: 16 } },
                      _vm._l(_vm.product.categories, function(cat) {
                        return _c(
                          "span",
                          [
                            _c(
                              "el-tag",
                              {
                                staticStyle: { "margin-right": "5px" },
                                attrs: { effect: "dark" }
                              },
                              [_vm._v(_vm._s(cat.name))]
                            )
                          ],
                          1
                        )
                      }),
                      0
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c("el-divider"),
                _vm._v(" "),
                _c(
                  "el-row",
                  { staticStyle: { "text-aling": "right" } },
                  [
                    _c(
                      "el-button",
                      {
                        attrs: { type: "success" },
                        on: {
                          click: function($event) {
                            return _vm.addToCart(_vm.product.id)
                          }
                        }
                      },
                      [_vm._v("Agregar al carrito")]
                    )
                  ],
                  1
                )
              ],
              1
            )
          ],
          1
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Shared/Layout.vue?vue&type=template&id=6bf30086&":
/*!*****************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/Shared/Layout.vue?vue&type=template&id=6bf30086& ***!
  \*****************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "el-container",
    { staticStyle: { height: "100%" } },
    [
      _c(
        "el-header",
        [
          _c("inertia-link", { attrs: { href: "/" } }, [
            _c(
              "span",
              {
                staticClass: "page-title",
                staticStyle: { "font-size": "40px" }
              },
              [_vm._v("Innova")]
            )
          ]),
          _vm._v(" "),
          _c(
            "el-row",
            { staticClass: "fa-pull-right" },
            [
              _c(
                "el-dropdown",
                { attrs: { trigger: "click" } },
                [
                  _c("avatar", {
                    attrs: {
                      username: _vm.name,
                      size: 40,
                      "background-color": "#9615db",
                      color: "#ca8aec"
                    }
                  }),
                  _vm._v(" "),
                  _c(
                    "el-dropdown-menu",
                    { attrs: { slot: "dropdown" }, slot: "dropdown" },
                    [
                      _c(
                        "a",
                        { attrs: { href: "/logout" } },
                        [_c("el-dropdown-item", [_vm._v("Salir")])],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "a",
                        { attrs: { href: "/cart" } },
                        [_c("el-dropdown-item", [_vm._v("Carrito de compra")])],
                        1
                      )
                    ]
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("el-header", {
        staticStyle: { height: "3px", "background-color": "#9615db" }
      }),
      _vm._v(" "),
      _vm.role === 1
        ? _c(
            "el-menu",
            {
              attrs: {
                mode: "horizontal",
                "background-color": "#545c64",
                "text-color": "#fff",
                "active-text-color": "#ca8aec"
              }
            },
            [
              _c("el-menu-item", { attrs: { index: "1" } }, [
                _c("a", { attrs: { href: "/brands" } }, [_vm._v("Marcas")])
              ]),
              _vm._v(" "),
              _c("el-menu-item", { attrs: { index: "2" } }, [
                _c("a", { attrs: { href: "/product" } }, [_vm._v("Productos")])
              ]),
              _vm._v(" "),
              _c("el-menu-item", { attrs: { index: "3" } }, [
                _c("a", { attrs: { href: "/categories" } }, [
                  _vm._v("Categorias")
                ])
              ]),
              _vm._v(" "),
              _c("el-menu-item", { attrs: { index: "4" } }, [
                _c("a", { attrs: { href: "/users" } }, [_vm._v("Usuarios")])
              ])
            ],
            1
          )
        : _vm._e(),
      _vm._v(" "),
      _c("el-container", [_c("el-main", [_vm._t("default")], 2)], 1)
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/Pages/Product/Show.vue":
/*!*********************************************!*\
  !*** ./resources/js/Pages/Product/Show.vue ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Show_vue_vue_type_template_id_6fc30034___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Show.vue?vue&type=template&id=6fc30034& */ "./resources/js/Pages/Product/Show.vue?vue&type=template&id=6fc30034&");
/* harmony import */ var _Show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Show.vue?vue&type=script&lang=js& */ "./resources/js/Pages/Product/Show.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Show_vue_vue_type_template_id_6fc30034___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Show_vue_vue_type_template_id_6fc30034___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/Product/Show.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/Product/Show.vue?vue&type=script&lang=js&":
/*!**********************************************************************!*\
  !*** ./resources/js/Pages/Product/Show.vue?vue&type=script&lang=js& ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Show.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Product/Show.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/Pages/Product/Show.vue?vue&type=template&id=6fc30034&":
/*!****************************************************************************!*\
  !*** ./resources/js/Pages/Product/Show.vue?vue&type=template&id=6fc30034& ***!
  \****************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_template_id_6fc30034___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Show.vue?vue&type=template&id=6fc30034& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Product/Show.vue?vue&type=template&id=6fc30034&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_template_id_6fc30034___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_template_id_6fc30034___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/Shared/Layout.vue":
/*!****************************************!*\
  !*** ./resources/js/Shared/Layout.vue ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Layout_vue_vue_type_template_id_6bf30086___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Layout.vue?vue&type=template&id=6bf30086& */ "./resources/js/Shared/Layout.vue?vue&type=template&id=6bf30086&");
/* harmony import */ var _Layout_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Layout.vue?vue&type=script&lang=js& */ "./resources/js/Shared/Layout.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Layout_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Layout_vue_vue_type_template_id_6bf30086___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Layout_vue_vue_type_template_id_6bf30086___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Shared/Layout.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/Shared/Layout.vue?vue&type=script&lang=js&":
/*!*****************************************************************!*\
  !*** ./resources/js/Shared/Layout.vue?vue&type=script&lang=js& ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Layout.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Shared/Layout.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/Shared/Layout.vue?vue&type=template&id=6bf30086&":
/*!***********************************************************************!*\
  !*** ./resources/js/Shared/Layout.vue?vue&type=template&id=6bf30086& ***!
  \***********************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_template_id_6bf30086___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Layout.vue?vue&type=template&id=6bf30086& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Shared/Layout.vue?vue&type=template&id=6bf30086&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_template_id_6bf30086___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_template_id_6bf30086___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);