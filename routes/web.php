<?php

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login')->name('login.attempt')->uses('Auth\LoginController@login');

Auth::routes();


Route::group(['middleware' => 'auth'], function () {
    Route::resource('/', 'DashController');
    Route::get('/logout')->name('logout')->uses('Auth\LoginController@logout');

    Route::resource('/users', 'UserController');
    Route::resource('/brands', 'BrandController');
    Route::resource('/categories', 'CategoryController');
    Route::resource('/product', 'ProductController');
    Route::post('/product/{id}', 'ProductController@update');



    Route::get('/cart', 'DashController@cart');
    Route::post('/cart/{id}', 'DashController@saveToCart');


});
